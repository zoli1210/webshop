import { CartItem } from './cart-item';

export class Order {
  private firstName!: string;
  private lastName!: string;
  private email!: string;
  private country!: string;
  private zipCode!: string;
  private city!: string;
  private streetAndHouseNumber!: string;
  private other!: string;
  private totalPrice!: number;
  private totalQuantity!: number;
  private paymentMethod!: string;
  private orderItems!: CartItem[];

  constructor(
    cartItems: CartItem[],
    firstName: string,
    lastName: string,
    email: string,
    country: string,
    zipCode: string,
    city: string,
    streetAndHouseNumber: string,
    other: string,
    totalPrice: number,
    totalQuantity: number,
    paymentMethod: string
  ) {
    this.orderItems = cartItems;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.country = country;
    this.zipCode = zipCode;
    this.city = city;
    this.streetAndHouseNumber = streetAndHouseNumber;
    this.other = other;
    this.totalPrice = totalPrice;
    this.totalQuantity = totalQuantity;
    this.paymentMethod = paymentMethod;
  }
}
