export class Product {
  id!: number;
  category!: string;
  sku!: string;
  name!: string;
  description!: string;
  unitPrice!: number;
  imageUrl!: string;
  active!: boolean;
  unitsInStock!: number;
  dateCreated!: Date;
  lastUpdated!: Date;
}
