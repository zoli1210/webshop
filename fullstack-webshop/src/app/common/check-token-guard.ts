import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { CookieData } from './cookie-data';

@Injectable({
  providedIn: 'root',
})
export class CheckTokenGuard implements CanActivate {
  accessToken: string;
  customerId: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private cookieService: CookieService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):| boolean| UrlTree| Observable<boolean | UrlTree>| Promise<boolean | UrlTree> {

    const userData: CookieData = {
      accessToken: this.cookieService.get('accessToken'),
    };

    this.authService.handleCookie(userData).subscribe({
      next: (response) => {
        if (response == false) {
          this.router.navigate(['/home']);
          console.log('Please sign in first!');
        }
      },
      error: (err) => {
        this.router.navigate(['/home']);
        return false;
      },
    });
    return true;
  }
}
