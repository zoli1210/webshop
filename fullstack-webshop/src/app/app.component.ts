import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'fullstack-webshop';

  constructor(
    private authService: AuthService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.refreshAuth();
  }

  refreshAuth() {
    const cookieToken = this.cookieService.get('accessToken');
    if (cookieToken != '') {
      this.authService.publishIsAuthenticatedNotification(true);
    }
  }
}
