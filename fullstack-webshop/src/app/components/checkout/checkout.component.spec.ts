import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';
import { CheckoutService } from 'src/app/services/checkout.service';
import { ProductsComponent } from '../products/products.component';
import Spy = jasmine.Spy;
import { CheckoutComponent } from './checkout.component';

class CartServiceFake{
  public addToCart(): void{}
  public computeCartTotals(): void{}
  public persistCartItems():void{}
  public logCartData():void {}
  public decrementQuantity(): void {}
  public remove(): void{}
  public totalPrice :Subject<number> = new BehaviorSubject<number>(0);
  public totalQuantity :Subject<number> = new BehaviorSubject<number>(0);
}

class CookieServiceFake {
  public get(): void {}
}
class CheckoutServiceFake{
  public handleOrder(): Observable<any> {
    return of(null);
  }
}
const setUpCheckoutForm = (): FormGroup => {
  const builder: FormBuilder = new FormBuilder();

  return builder.group({
    userData: builder.group({
      firstName: new FormControl(),
      lastName: new FormControl(),
      email: new FormControl(),
      city: new FormControl(),
      streetAndHouseNumber: new FormControl(),
      other: new FormControl(),
      zipCode: new FormControl(),
      coutry: new FormControl(),
    }),
    paymentMethod: builder.group({
      meansOfPayment: new FormControl()
    })
  });
};

fdescribe('CheckoutComponent', () => {
  let component: CheckoutComponent;
  let fixture: ComponentFixture<CheckoutComponent>;

  const cartServiceFake: CartServiceFake = new CartServiceFake();
  const cookieServiceFake: CookieServiceFake = new CookieServiceFake();
  const checkoutServiceFake: CheckoutServiceFake =new CheckoutServiceFake();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'products', component: ProductsComponent },
        ]),
      ],
      declarations: [ CheckoutComponent ],
      providers: [
        {
          provide: CartService,
          useValue: cartServiceFake,
        },
        {
          provide: CookieService,
          useValue: cookieServiceFake,
        },
        {
          provide: CheckoutService,
          useValue: checkoutServiceFake,
        }
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutComponent);
    component = fixture.componentInstance;
    component.checkoutForm = setUpCheckoutForm();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call handleOrder', () => {
    const customerForm = component.checkoutForm.get('customer') as FormGroup;

    customerForm.setValue({
        firstName: "Szabo",
        lastName: "Laszlo",
        email: "szabolaszlo@gmail.com",
        city: "Miskolc",
        streetAndHouseNumber: "VÖrösmarty utca 20 3/3",
        other: "",
        zipCode: "3525",
        country:  "Hungary"
    });

    const paymentForm = component.checkoutForm.get('paymentMethod') as FormGroup;

    paymentForm.setValue({
      meansOfPayment: "PayPal"
    })

    const checkoutSpy: Spy = spyOn(
      checkoutServiceFake,
      'handleOrder'
    ).and.callThrough();

    component.onSubmit();

    expect(checkoutSpy).toHaveBeenCalled();
  });
  
  it('should call handleOrder', () => {
    const customerForm = component.checkoutForm.get('customer') as FormGroup;

    customerForm.setValue({
        firstName: "Szabo",
        lastName: "Laszlo",
        email: "szabolaszlomail.com",
        city: "Miskolc",
        streetAndHouseNumber: "VÖrösmarty utca 20 3/3",
        other: "",
        zipCode: "3525",
        country:  ""
    });

    const paymentForm = component.checkoutForm.get('paymentMethod') as FormGroup;

    paymentForm.setValue({
      meansOfPayment: "PayPal"
    })

    const checkoutSpy: Spy = spyOn(
      checkoutServiceFake,
      'handleOrder'
    ).and.callThrough();

    component.onSubmit();

    expect(checkoutSpy).not.toHaveBeenCalled();
  });
});
