import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { CartItem } from 'src/app/common/cart-item';
import { Order } from 'src/app/common/order';
import { Product } from 'src/app/common/product';
import { CartService } from 'src/app/services/cart.service';
import { CheckoutService } from 'src/app/services/checkout.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  cartItems: CartItem[] = [];

  checkoutForm!: FormGroup;

  totalPrice: number = 0;
  totalQuantity: number = 0;

  creditCardYears: number[] = [];
  creditCardMonths: number[] = [];

  paymentValue: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private cartService: CartService,
    private checkoutService: CheckoutService,
    private router: Router,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.reviewCartDetails();

    this.checkoutForm = this.formBuilder.group({
      customer: this.formBuilder.group({
        firstName: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
        ]),
        lastName: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
        ]),
        email: new FormControl('', [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._%+-]+\\.[a-z]{2,4}$'),
        ]),
        city: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
        ]),
        streetAndHouseNumber: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
        ]),
        other: [''],
        zipCode: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
        ]),
        country: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
        ]),
      }),
      paymentMethod: this.formBuilder.group({
        meansOfPayment: new FormControl('')
      }),
    });
  }

  onSwitchMode() {
    let meansOfPayment =
      this.checkoutForm.value.paymentMethod.meansOfPayment;

    if (meansOfPayment === 'Cash on delivery') {
      this.paymentValue = false;
    } else {
      this.paymentValue = true;
    }
  }

  get firstName() {
    return this.checkoutForm.get('customer.firstName');
  }
  get lastName() {
    return this.checkoutForm.get('customer.lastName');
  }
  get email() {
    return this.checkoutForm.get('customer.email');
  }
  get streetAndHouseNumber() {
    return this.checkoutForm.get('customer.streetAndHouseNumber');
  }
  get zipCode() {
    return this.checkoutForm.get('customer.zipCode');
  }
  get country() {
    return this.checkoutForm.get('customer.country');
  }

  reviewCartDetails() {
    this.cartItems = this.cartService.cartItems;

    this.cartService.totalQuantity.subscribe(
      (totalQuantity) => (this.totalQuantity = totalQuantity)
    );

    this.cartService.totalPrice.subscribe(
      (totalPrice) => (this.totalPrice = totalPrice)
    );
  }

  onSubmit() {
    if (this.checkoutForm.invalid) {
      this.checkoutForm.markAllAsTouched();
      return;
    }

    const checkoutCustomerValues = this.checkoutForm.value.customer;

    let order = new Order(
      this.cartItems,
      checkoutCustomerValues.firstName,
      checkoutCustomerValues.lastName,
      checkoutCustomerValues.email,
      checkoutCustomerValues.country,
      checkoutCustomerValues.zipCode,
      checkoutCustomerValues.city,
      checkoutCustomerValues.streetAndHouseNumber,
      checkoutCustomerValues.other,
      this.totalPrice,
      this.totalQuantity,
      this.checkoutForm.value.paymentMethod.meansOfPayment
    );

    const cookieAccessToken = this.cookieService.get('accessToken');
    const cookieEmail = this.cookieService.get('email');

    const headersData = new HttpHeaders()
      .set('accessToken', cookieAccessToken)
      .set('email', cookieEmail);

    this.checkoutService.handleOrder(order, headersData).subscribe({
      next: (response) => {
        alert('Your order has been recieved');

        this.resetCart();
      },
      error: (err) => {
        alert(`There was an error: ${err.message}`);
      },
    });
  }

  resetCart() {
    this.cartService.cartItems = [];
    this.cartService.totalPrice.next(0);
    this.cartService.totalQuantity.next(0);

    this.checkoutForm.reset();

    this.router.navigateByUrl('/products');
  }
}
