import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/common/user';
import { RegistrationService } from 'src/app/services/registration.service';
import { ConfirmedValidator } from './confirmed.validator';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  registrationForm!: FormGroup;

  emailExist: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private registrationService: RegistrationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.registrationForm = this.formBuilder.group({
      userData: this.formBuilder.group(
        {
          firstName: new FormControl('', [
            Validators.required,
            Validators.minLength(3),
          ]),
          lastName: new FormControl('', [
            Validators.required,
            Validators.minLength(3),
          ]),
          email: new FormControl('', [
            Validators.required,
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._%+-]+\\.[a-z]{2,4}$'),
          ]),
          password: new FormControl('', [
            Validators.required,
            Validators.minLength(6),
          ]),
          confirmPassword: new FormControl('', Validators.required),
        },
        {
          validator: ConfirmedValidator('password', 'confirmPassword'),
        }
      ),
    });
  }

  get firstName() {
    return this.registrationForm.get('userData.firstName');
  }
  get lastName() {
    return this.registrationForm.get('userData.lastName');
  }
  get email() {
    return this.registrationForm.get('userData.email');
  }
  get password() {
    return this.registrationForm.get('userData.password');
  }
  get confirmPassword() {
    return this.registrationForm.get('userData.confirmPassword');
  }

  checkEmailExist() {
    let email: string = this.registrationForm.value.userData.email;

    this.registrationService.handleEmailExist(email).subscribe({
      next: (response) => {
        response ? (this.emailExist = true) : (this.emailExist = false);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  onSubmit() {
    if (this.registrationForm.invalid) {
      this.registrationForm.markAllAsTouched();
      return;
    }

    const userData = this.registrationForm.value.userData;

    const user: User = {
      firstName: userData.firstName,
      lastName: userData.lastName,
      email: userData.email,
      password: userData.password,
    };

    this.registrationService
      .handleRegistration(user)
      .subscribe(this.successRegHandler, this.errorRegHandler);
  }

  private readonly successRegHandler = (user: User): void => {
    alert('Registration was success!');
    this.resetForm();
  };

  private readonly errorRegHandler = (err: Error): void => {
    alert(`There was an error: ${err.message}`);
  };

  resetForm() {
    this.registrationForm.reset();
    this.router.navigateByUrl('/login');
  }
}
