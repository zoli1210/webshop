import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import Spy = jasmine.Spy;

import { Observable, of } from 'rxjs';
import { RegistrationService } from 'src/app/services/registration.service';
import { AuthComponent } from '../auth/auth.component';

import { RegistrationComponent } from './registration.component';

class RegistrationServiceFake {
  public handleRegistration(): Observable<any> {
    return of(null);
  }
  public handleEmailExist(): Observable<any> {
    return of(null);
  }
}

const setUpRegistrationForm = (): FormGroup => {
  const builder: FormBuilder = new FormBuilder();

  return builder.group({
    userData: builder.group({
      firstName: new FormControl(),
      lastName: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
      confirmPassword: new FormControl(),
    }),
  });
};

fdescribe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;

  const registrationServiceFake: RegistrationServiceFake =
    new RegistrationServiceFake();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'login', component: AuthComponent },
        ]),
      ],
      declarations: [RegistrationComponent],
      providers: [
        {
          provide: RegistrationService,
          useValue: registrationServiceFake,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    component.registrationForm = setUpRegistrationForm();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call handleRegistration', () => {
    const userDataForm = component.registrationForm.get('userData') as FormGroup;

    userDataForm.setValue({
      firstName: 'Zoltan',
      lastName: 'Papp',
      email: 'zoli96@gmail.com',
      password: 'abcdasd12',
      confirmPassword: 'abcdasd12'
    });

    const registrationSpy: Spy = spyOn(
      registrationServiceFake,
      'handleRegistration'
    ).and.callThrough();

    component.onSubmit();

    expect(registrationSpy).toHaveBeenCalled();
  });
  it('should not have been called', () => {
    const userDataForm = component.registrationForm.get('userData') as FormGroup;

    userDataForm.setValue({
      firstName: 'Zoltan',
      lastName: 'Papp',
      email: 'zoli96mail.com',
      password: 'abcdasd12',
      confirmPassword: 'abcdasd12'
    });

    const registrationSpy: Spy = spyOn(
      registrationServiceFake,
      'handleRegistration'
    ).and.callThrough();

    component.onSubmit();

    expect(registrationSpy).not.toHaveBeenCalled();
  });
});
