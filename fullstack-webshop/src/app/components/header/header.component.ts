import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { CookieService } from 'ngx-cookie-service';
import { Subject, Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { AuthComponent } from '../auth/auth.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  faSearch = faSearch;

  isAuthenticated: boolean = false;

  private isAuthenticatedSubscription: Subscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private cookieService: CookieService
  ) {
    this.isAuthenticatedSubscription =
      this.authService.isAuthenticatedNotification.subscribe(this.authHandler);
  }

  ngOnInit(): void {}

  logOut() {
    this.isAuthenticated = false;
    this.cookieService.deleteAll();
    this.router.navigate(['/login']);
  }

  private readonly authHandler = (isAuth: boolean): void => {
    this.isAuthenticated = isAuth;
  };

  ngOnDestroy(): void {
    this.isAuthenticatedSubscription.unsubscribe();
  }
}
