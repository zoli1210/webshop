import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { Auth } from 'src/app/common/auth';
import { AuthService } from 'src/app/services/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  authForm!: FormGroup;
  storage: Storage = sessionStorage;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.authForm = this.formBuilder.group({
      user: this.formBuilder.group({
        email: new FormControl('', [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._%+-]+\\.[a-z]{2,4}$'),
        ]),
        password: new FormControl('', Validators.required),
      }),
    });
  }

  onSubmit() {
    if (this.authForm.invalid) {
      return;
    }
    const userData = this.authForm.value.user;

    const user: Auth = {
      email: userData.email,
      password: userData.password,
    };

    console.log(user);

    this.authService.handleLogin(user).subscribe({
      next: (response) => {
        this.cookieService.set('accessToken', response.accessToken, 1);
        this.cookieService.set('email', user.email, 1);
        this.authService.publishIsAuthenticatedNotification(true);
        this.router.navigate(['/home']);
        alert('Login was successfull');
      },
      error: (err) => {
        alert('Authentication was failed!');
      },
    });
  }
}
