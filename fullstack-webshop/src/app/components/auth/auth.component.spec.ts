import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieService } from 'ngx-cookie-service';
import { Observable, of } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import Spy = jasmine.Spy;

import { AuthComponent } from './auth.component';
import { HomeComponent } from '../home/home.component';

class AuthServiceFake {
  public handleLogin(): Observable<any> {
    return of(null);
  }

  public handleCookie(): Observable<any> {
    return of(null);
  }

  public isAuthenticatedNotification(): Observable<boolean> {
    return of(false);
  }

  public publishIsAuthenticatedNotification(isAuth: boolean): void {}
}

class CookieServiceFake {
  public set(): void {}
}

const setUpAuthForm = (): FormGroup => {
  const builder: FormBuilder = new FormBuilder();

  return builder.group({
    user: builder.group({
      email: new FormControl(),
      password: new FormControl(),
    }),
  });
};

fdescribe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;

  const authServiceFake: AuthServiceFake = new AuthServiceFake();
  const cookieServiceFake: CookieServiceFake = new CookieServiceFake();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'home', component: HomeComponent },
        ]),
      ],
      declarations: [AuthComponent],
      providers: [
        {
          provide: AuthService,
          useValue: authServiceFake,
        },
        {
          provide: CookieService,
          useValue: cookieServiceFake,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    component.authForm = setUpAuthForm();
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call handleLogin', () => {
    const userForm = component.authForm.get('user') as FormGroup;

    userForm.setValue({
      email: 'zoli96@gmail.com',
      password: 'abcdasd12',
    });

    const authLoginSpy: Spy = spyOn(
      authServiceFake,
      'handleLogin'
    ).and.callThrough();

    component.onSubmit();

    expect(authLoginSpy).toHaveBeenCalled();
  });

  it('should not have been called', () => {
    const userForm = component.authForm.get('user') as FormGroup;

    userForm.setValue({
      email: 'abcd.com',
      password: 'ah23',
    });

    const autLoginSpy: Spy = spyOn(
      authServiceFake,
      'handleLogin'
    ).and.callThrough();

    component.onSubmit();

    expect(autLoginSpy).not.toHaveBeenCalled();
  });

  it('should call cookie with correct access token', () => {
    const userForm = component.authForm.get('user') as FormGroup;

    userForm.setValue({
      email: 'zoli96@gmail.com',
      password: 'abcdasd12',
    });

    const autLoginSpy: Spy = spyOn(authServiceFake, 'handleLogin').and.callFake(
      () => {
        return of({
          accessToken: '123456',
        });
      }
    );

    const cookieSpy: Spy = spyOn(cookieServiceFake, 'set').and.callThrough();

    component.onSubmit();

    expect(cookieSpy).toHaveBeenCalledWith('accessToken', '123456', 1);
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });
});
