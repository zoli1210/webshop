import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { Order } from '../common/order';

@Injectable({
  providedIn: 'root',
})
export class CheckoutService {
  orderUrl = 'http://localhost:8080/api/order';

  constructor(private httpClient: HttpClient) {}

  handleOrder(orderData: Order, headersData: HttpHeaders): Observable<any> {
    return this.httpClient.post<Order>(this.orderUrl, orderData, {
      headers: headersData,
    });
  }
}
