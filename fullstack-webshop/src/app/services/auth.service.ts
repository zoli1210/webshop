import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Auth } from '../common/auth';
import { CookieData } from '../common/cookie-data';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  loginUrl = 'http://localhost:8080/api/login';

  cookieUrl = 'http://localhost:8080/api/cookieData/';

  private isAuthenticated: Subject<boolean> = new Subject<boolean>();

  constructor(private httpClient: HttpClient) {}

  handleLogin(userData: Auth): Observable<any> {
    return this.httpClient.post<Auth>(this.loginUrl, userData);
  }

  handleCookie(cookieData: CookieData): Observable<any> {
    return this.httpClient.get(this.cookieUrl + cookieData.accessToken);
  }

  public get isAuthenticatedNotification(): Observable<boolean> {
    return this.isAuthenticated.asObservable();
  }

  public publishIsAuthenticatedNotification(isAuth: boolean): void {
    this.isAuthenticated.next(isAuth);
  }
}
