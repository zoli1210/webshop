import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { User } from '../common/user';

@Injectable({
  providedIn: 'root',
})
export class RegistrationService {
  registrationUrl = 'http://localhost:8080/api/registrationData';

  emailExistUrl = 'http://localhost:8080/api/emailExist';

  constructor(private httpClient: HttpClient) {}

  handleRegistration(userData: User): Observable<any> {
    return this.httpClient.post<User>(this.registrationUrl, userData);
  }

  handleEmailExist(email: string): Observable<any> {
    return this.httpClient.post(this.emailExistUrl, email);
  }
}
