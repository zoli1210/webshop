import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map, Observable } from 'rxjs';
import { Product } from '../common/product';
import { ProductCategory } from '../common/product-category';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private localhostUrl = 'http://localhost:8080';

  private baseUrl = `${this.localhostUrl}/api`;

  private categoryPath = '/product-categories';

  private productPath = '/products';

  constructor(private http: HttpClient) {}

  private getProducts(searchUrl: string): Observable<Product[]> {
    return this.http.get<Product[]>(searchUrl);
  }

  getProductList(categoryId: number): Observable<Product[]> {
    const searchUrl = `${
      this.baseUrl + this.productPath + `?category-id=${categoryId}`
    }`;

    return this.getProducts(searchUrl);
  }

  getProductCategories(): Observable<ProductCategory[]> {
    return this.http.get<ProductCategory[]>(this.baseUrl + this.categoryPath);
  }

  getProduct(productId: number): Observable<Product> {
    const productUrl = `${this.baseUrl + this.productPath + `/${productId}`}`;

    return this.http.get<Product>(productUrl);
  }
}
